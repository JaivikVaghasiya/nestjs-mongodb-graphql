// const { loadFilesSync } = require('@graphql-tools/load-files');
// const { mergeTypeDefs } = require('@graphql-tools/merge');
// const { print } = require('graphql');
// const fs = require('fs');

// const loadedFiles = loadFilesSync(`./src/**/*.graphql`);
// const typeDefs = mergeTypeDefs(loadedFiles);
// const printedTypeDefs = print(typeDefs);
// console.log('printedTypeDefs :>> ', printedTypeDefs);
// fs.writeFileSync('schema.graphql', printedTypeDefs);

const {
  getIntrospectionQuery,
  buildClientSchema,
  printSchema,
} = require('graphql');
const fs = require('fs');
const fetch = require('node-fetch');

const endpoint = 'http://localhost:8000/graphql'; // Replace with your GraphQL endpoint

async function generateSchema() {
  try {
    // Fetch the GraphQL schema using introspection
    const response = await fetch(endpoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ query: getIntrospectionQuery() }),
    });

    const result = await response.json();

    if (result.errors) {
      throw new Error(result.errors[0].message);
    }

    // Build the GraphQL schema from the introspection result
    const schema = buildClientSchema(result.data);

    // Convert the schema to SDL (Schema Definition Language)
    const schemaSDL = printSchema(schema);

    // Save the SDL to a file
    fs.writeFileSync('./api/schema.graphql', schemaSDL);

    console.log('Schema file generated successfully: schema.graphql');
  } catch (error) {
    console.error('Error generating schema:', error.message);
  }
}

generateSchema();
