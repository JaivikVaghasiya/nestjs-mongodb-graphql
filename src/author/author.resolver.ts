import {
  Args,
  Resolver,
  Query,
  ResolveField,
  Parent,
  Mutation,
  Subscription,
  Context,
} from '@nestjs/graphql';
import { AuthorService } from './author.service';
import { PubSub } from 'graphql-subscriptions';
import { BadRequestException, UseGuards } from '@nestjs/common';
import { AuthGuard, Roles, RolesGuard } from 'src/auth/auth.guard';
import { Logger } from 'src/utils/logger';
import { Services } from 'src/utils/services';

const pubSub = new PubSub();
@Resolver('Author')
export class AuthorResolver {
  readonly logger = Logger.Author;

  constructor(
    private authorsService: AuthorService,
    private services: Services,
  ) {
    this.authorsService.setLogger(this.logger);
    this.services.setLogger(this.logger);
  }

  @Query('author')
  @UseGuards(AuthGuard, RolesGuard)
  @Roles('admin')
  author(@Args('id') id: number, @Context() context: any) {
    console.log('context :>> ', context);
    const data = this.authorsService.findOne(id);
    console.log('data :>> ', data);
    return data;
  }

  @ResolveField('posts')
  posts(@Parent() author) {
    console.log('author1 :>> ', author);
    const response = this.authorsService.findAll(author);
    console.log('response :>> ', response);
    return response;
  }

  @Mutation()
  authorPost(@Args() input: any) {
    console.log('input :>> ', input);
    this.logger.error(input, 'input');
    throw new BadRequestException('Something went wrong');
    // console.log('args :>> ', args);
    // const response = {
    //   id: args.postId,
    //   title: 'String!',
    //   votes: 1,
    // };
    pubSub.publish('commentAdded', { commentAdded: [] });
    return [];
  }

  @Subscription('commentAdded', {
    filter: (payload, variables) => {
      console.log('payload :>> ', payload);
      console.log('variables :>> ', variables);
      return payload.commentAdded.id === variables.postId;
    },
  })
  commentAdded() {
    return pubSub.asyncIterator('commentAdded');
  }
}
