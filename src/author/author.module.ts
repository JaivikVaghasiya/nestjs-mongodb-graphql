import { Module } from '@nestjs/common';
import { AuthorService } from './author.service';
import { AuthorResolver } from './author.resolver';
import { Services } from 'src/utils/services';

@Module({
  providers: [AuthorService, AuthorResolver, Services],
  exports: [AuthorService],
})
export class AuthorModule {}
