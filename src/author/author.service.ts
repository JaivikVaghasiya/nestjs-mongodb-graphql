import { Injectable } from '@nestjs/common';
import { Logger } from 'src/utils/logger';
@Injectable()
export class AuthorService {
  private logger: Logger;
  setLogger(logger: Logger) {
    console.log('this.logger :>> ', this.logger);
    this.logger = logger;
  }
  findAll(author) {
    return [
      {
        id: author.id + 1,
        title: 'author.title',
        votes: 22,
      },
    ];
  }

  findOne(id: number) {
    this.logger.info(id);
    return {
      id: id,
      firstName: 'String12121',
      lastName: 'String',
      posts: [
        {
          id,
          title: 'String!',
          votes: id,
        },
      ],
    };
  }
}
