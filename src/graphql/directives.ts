// import { mapSchema, getDirective, MapperKind } from '@graphql-tools/utils';
// import { Injectable, UnauthorizedException } from '@nestjs/common';
// import { JwtService } from '@nestjs/jwt';
// import { defaultFieldResolver } from 'graphql';

// // const auth = async (headers: any) => {
// //   const [, token] = headers.authorization?.split(' ') ?? [];
// //   if (!token) throw new UnauthorizedException();
// //   try {
// //     const payload = JwtService.verifyAsync(token, {
// //       secret: process.env.JWT_SECRET,
// //     });
// //     return payload;
// //   } catch {
// //     throw new UnauthorizedException();
// //   }
// // };

// export function authDirective(schema) {
//   const typeDirectiveArgumentMaps = {};
//   return mapSchema(schema, {
//     [MapperKind.OBJECT_FIELD]: (fieldConfig, _fieldName, typeName) => {
//       const authDirective =
//         getDirective(schema, fieldConfig, 'auth')?.[0] ??
//         typeDirectiveArgumentMaps[typeName];
//       if (authDirective) {
//         const { roles } = authDirective;
//         if (roles) {
//           const { resolve = defaultFieldResolver } = fieldConfig;
//           fieldConfig.resolve = async function (source, args, context, info) {
//             // try {
//             await auth(context.req.headers);
//             return resolve(source, args, context, info);
//             // } catch (error) {
//             //   throw error;
//             // }
//           };
//           return fieldConfig;
//         }
//       }
//     },
//   });
// }

// @Injectable()
// export class directives {
//   constructor(private jwtService: JwtService) {}

//   authDirective(schema) {
//     const typeDirectiveArgumentMaps = {};
//     return mapSchema(schema, {
//       [MapperKind.OBJECT_FIELD]: (fieldConfig, _fieldName, typeName) => {
//         const authDirective =
//           getDirective(schema, fieldConfig, 'auth')?.[0] ??
//           typeDirectiveArgumentMaps[typeName];
//         if (authDirective) {
//           const { roles } = authDirective;
//           if (roles) {
//             const { resolve = defaultFieldResolver } = fieldConfig;
//             fieldConfig.resolve = async function (source, args, context, info) {
//               // try {
//               await auth(context.req.headers);
//               return resolve(source, args, context, info);
//               // } catch (error) {
//               //   throw error;
//               // }
//             };
//             return fieldConfig;
//           }
//         }
//       },
//     });
//   }
// }
