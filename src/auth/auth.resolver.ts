import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { CreateAuthInput } from './dto/create-auth.input';
import { UpdateAuthInput } from './dto/update-auth.input';
import { BadRequestException, UseGuards } from '@nestjs/common';
import { AuthGuard } from './auth.guard';
import { Logger } from 'src/utils/logger';

@Resolver('Auth')
export class AuthResolver {
  private readonly logger = new Logger('auth');
  constructor(private readonly authService: AuthService) {}

  @Mutation('createAuth')
  create(@Args('createAuthInput') createAuthInput: CreateAuthInput) {
    this.logger.info(createAuthInput, 'createAuthInput');
    throw new BadRequestException('throw an error');
    return this.authService.create(createAuthInput);
  }
  // // @Query('auth')
  // findAll() {
  //   return this.authService.findAll();
  // }
  @Query('auth')
  @UseGuards(AuthGuard)
  findOne(@Args('id') id: number) {
    console.log('id :>> ', id);
    console.log('process.env.test :>> ', process.env.JWT_SECRET);
    return this.authService.findOne(id);
  }

  @Mutation('updateAuth')
  update(@Args('updateAuthInput') updateAuthInput: UpdateAuthInput) {
    return this.authService.update(updateAuthInput.id, updateAuthInput);
  }

  @Mutation('removeAuth')
  remove(@Args('id') id: number) {
    return this.authService.remove(id);
  }
}
