import { mapSchema, getDirective, MapperKind } from '@graphql-tools/utils';
import { UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { defaultFieldResolver } from 'graphql';
import { Logger } from 'src/utils/logger';

export class Directives {
  private jwtService: JwtService;
  private logger: any;

  constructor() {
    this.jwtService = new JwtService();
  }
  functionDirective(schema) {
    const typeDirectiveArgumentMaps = {};
    return mapSchema(schema, {
      [MapperKind.OBJECT_FIELD]: (fieldConfig, _fieldName, typeName) => {
        console.log('_fieldName :>> ', _fieldName);
        const functionDirective =
          getDirective(schema, fieldConfig, 'auth')?.[0] ??
          typeDirectiveArgumentMaps[typeName];
        if (functionDirective) {
          const { name, role } = functionDirective;
          const { resolve = defaultFieldResolver } = fieldConfig;
          if (role) {
            fieldConfig.resolve = async function (source, args, context, info) {
              try {
                this.logger = Logger[name];
                this.logger.info('auther >>>>>');
                this.auth(context.req.headers, role);
                const fn = resolve(source, args, context, info);
                return await Promise.resolve(fn).catch();
              } catch (error) {
                this.logger.error(error, 'error');
                return {
                  statusCode: error?.status || 500,
                  message: error.message,
                };
              }
            }.bind(this);
            return fieldConfig;
          }
          fieldConfig.resolve = async function (source, args, context, info) {
            try {
              this.logger.info('without auth');
              const fn = resolve(source, args, context, info);
              return await Promise.resolve(fn).catch();
            } catch (error) {
              this.logger.error(error, 'error');
              return {
                statusCode: error?.status || 500,
                message: error.message,
              };
            }
          }.bind(this);
          return fieldConfig;
        }
      },
    });
  }

  private auth(headers, roles: string[]): any {
    const [, token] = headers.authorization?.split(' ') ?? [];
    if (!token) throw new UnauthorizedException();
    try {
      const payload: any = this.jwtService.verifyAsync(token, {
        secret: process.env.JWT_SECRET,
      });
      if (!roles.includes(payload.role)) throw new UnauthorizedException();
      return payload;
    } catch {
      throw new UnauthorizedException();
    }
  }
}
