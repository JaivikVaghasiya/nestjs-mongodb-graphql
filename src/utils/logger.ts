import {
  transports,
  Logger as WinstonLogger,
  createLogger as CreateLogger,
  format,
} from 'winston';
import { join } from 'path';

class Logger {
  private logger: WinstonLogger;
  private errorLogger: WinstonLogger;

  constructor(name: string) {
    this.logger = this.createLogger('info', name);
    this.errorLogger = this.createLogger('error', name);
  }

  log(level: string, log: any, key = 'data'): void {
    console.log(`${key} :>>`, log);
    this.logger.log(level, { key, log });
  }

  info(log: any, key = 'info'): void {
    console.log(`${key} :>>`, log);
    this.logger.info({ key, log });
  }

  error(log: any, key = 'error'): void {
    console.log(`${key} :>>`, log);
    this.errorLogger.error({ key, log });
  }

  private getFilePath(level: string, controller?: string): string {
    const currentDate = new Date().toLocaleDateString().replace(/\//g, '-');
    const modulePath = join('logs', currentDate, controller || 'unknown');
    return join(modulePath, `${level}.log`);
  }

  private commonFormat(level: string) {
    return format.combine(
      format.timestamp(),
      format.printf(
        ({ timestamp, message }) =>
          `${timestamp} [${level}] ${message.key} :>> ${this.formatMessage(
            message.log,
          )}`,
      ),
    );
  }

  private createLogger(level: string, name: string) {
    return CreateLogger({
      transports: [
        new transports.File({
          format: this.commonFormat(level),
          level,
          filename: this.getFilePath(level, name),
        }),
      ],
    });
  }

  private formatMessage(message: any): string {
    return typeof message === 'object'
      ? JSON.stringify(message, null, 2)
      : message;
  }

  static readonly Author = new this('Author');
  static readonly Auth = new this('Auth');
}

export { Logger };
